import React, { Component, Fragment } from "react";

import "./styles.scss";

import Navbar from "./components/Navbar";
import Sidebar from "./components/Sidebar";
import TagsProvider from "./contexts/TagsContext";
import CardsProvider from "./contexts/CardsContext";
import CardList from "./components/CardList";

class App extends Component {
  render() {
    return (
      <div className="App">
        <Navbar />

        <div className="content">
          <CardsProvider>
            <Fragment>
              <TagsProvider>
                <Sidebar />
              </TagsProvider>

              <CardList />
            </Fragment>
          </CardsProvider>
        </div>
      </div>
    );
  }
}

export default App;
