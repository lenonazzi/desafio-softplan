import React, { useState, createContext } from "react";
import api from "../api";

export const CardsContext = createContext({
  cards: api.cards
});

const CardsProvider = ({ children }) => {
  const [cards] = useState(api.cards);

  return (
    <CardsContext.Provider value={{ cards }}>{children}</CardsContext.Provider>
  );
};

export default CardsProvider;
