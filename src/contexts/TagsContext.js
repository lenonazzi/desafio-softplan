import React, { useState, createContext } from "react";
import api from "../api";

import randomColor from "randomcolor";

export const TagsContext = createContext();

const TagsProvider = ({ children }) => {
  const [tags, createTag] = useState(api.tags);

  const addTag = tag => {
    const newTag = {
      id: tags.length + 1,
      name: tag.title,
      background: randomColor()
    };

    createTag([...tags, newTag]);
  };

  return (
    <TagsContext.Provider value={{ tags, addTag }}>
      {children}
    </TagsContext.Provider>
  );
};

export default TagsProvider;
