import React, { useContext } from "react";
import { CardsContext } from "../../contexts/CardsContext";

import "./style.scss";

import Card from "../Card";

const CardList = ({ cards }) => {
  const context = useContext(CardsContext);

  const data = context.cards.map(card => <Card key={card.id} card={card} />);

  return <div className="cardlist">{data}</div>;
};

export default CardList;
