import React, { useContext } from "react";
import { TagsContext } from "../../contexts/TagsContext";

import "./style.scss";

const TagCreate = () => {
  const context = useContext(TagsContext);

  return (
    <div className="tag-list">
      <span className="tag-list__title">Etiquetas</span>

      <div className="tag-list__items">{data}</div>
    </div>
  );
};

export default TagCreate;
