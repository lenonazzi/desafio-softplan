import React, { Component } from "react";

import "./style.scss";
import IconLabel from "../IconLabel";

class Navbar extends Component {
  render() {
    return (
      <div className="navbar">
        <h1 className="title">APP</h1>

        <div className="avatar">
          <IconLabel icon="User.svg" />
        </div>
      </div>
    );
  }
}

export default Navbar;
