import React from "react";

import "./style.scss";

const Tag = ({ tag }) => {
  return (
    <div className="tag">
      <span className="" />
      {tag.name}
    </div>
  );
};

export default Tag;
