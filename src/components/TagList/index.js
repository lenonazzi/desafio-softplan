import React, { useContext } from "react";
import { TagsContext } from "../../contexts/TagsContext";

import "./style.scss";

import Tag from "../Tag";

const TagList = () => {
  const { tags, addTag } = useContext(TagsContext);

  const newTag = () => {
    addTag({
      title: "teste"
    });
  };

  return (
    <div className="tag-list">
      <span className="tag-list__title">Etiquetas</span>

      <div className="tag-list__items">
        {tags.map(tag => (
          <Tag key={tag.id} tag={tag} />
        ))}
      </div>

      <button onClick={newTag}>Create Tag</button>
    </div>
  );
};

export default TagList;
