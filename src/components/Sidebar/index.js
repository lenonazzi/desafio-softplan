import React from "react";

import "./style.scss";
import TagList from "../TagList";

const Sidebar = () => {
  return (
    <div className="sidebar">
      <TagList />

      <div className="sidebar-footer">Feitos</div>
    </div>
  );
};

export default Sidebar;
