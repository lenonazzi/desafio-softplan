import React from "react";

import "./style.scss";

const IconLabel = ({ icon, altText, children }) => {
  return (
    <div className="iconLabel">
      <img src={`/assets/icons/${icon}`} alt={altText} />

      {children}
    </div>
  );
};

export default IconLabel;
