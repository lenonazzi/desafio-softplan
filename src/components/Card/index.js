import React from "react";

import "./style.scss";

const Card = ({ card }) => {
  return (
    <div className="card">
      <div className="card-item">{card.classe}</div>
      <div className="card-item">{card.assunto}</div>
    </div>
  );
};

export default Card;
